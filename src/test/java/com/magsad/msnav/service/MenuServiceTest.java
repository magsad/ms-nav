package com.magsad.msnav.service;

import com.magsad.msnav.entity.Menu;
import com.magsad.msnav.exception.MenuNotFoundException;
import com.magsad.msnav.payload.dto.MenuDTO;
import com.magsad.msnav.payload.response.MessageResponse;
import com.magsad.msnav.repository.MenuRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BooleanSupplier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MenuServiceTest {
    @Mock
    private MenuRepository menuRepository;

    @InjectMocks
    private MenuService menuService;

    @Test
    void testObjectNotNull(){
        assertNotNull(menuService);
    }

    @Test
    void testGetAllSuccess(){
        when(menuRepository.findAll()).thenReturn(getList());

        List<Menu> menus = menuService.getAll();
        assertEquals(menus.size(),2);
    }

    List<Menu> getList(){
        List<Menu> menuList = new ArrayList<>();
        Menu menu1 = new Menu();
        Menu menu2 = new Menu();

        menuList.add(menu1);
        menuList.add(menu2);
        return menuList;
    }

    @Test
    void testGetByIdSuccess(){
        when(menuRepository.findById(1L)).thenReturn(getMenu());

        Menu menu = menuService.getById(1L);
        assertEquals(menu.getUrl(),"/");
        assertEquals(menu.getName(),"Root");
    }

    Optional<Menu> getMenu(){
        Menu menu = new Menu();
        menu.setId(1L);
        menu.setUrl("/");
        menu.setName("Root");
        menu.getChildren().add(menu);
        return Optional.of(menu);
    }

    @Test
    void testGetByIdNotFoundException(){
        when(menuRepository.findById(1L)).thenThrow(new MenuNotFoundException(1L));
        assertThrows(MenuNotFoundException.class,()->menuService.getById(1L));
    }

    @Test
    void testCreateMenuSuccess(){
        Menu menu = getMenu().get();
        MenuDTO menuDTO = new MenuDTO();
        menuDTO.setId(1L);
        menuDTO.setUrl("");
        menuDTO.setName("Root");
        menuDTO.setParentId(null);
        when(menuRepository.save(any())).thenReturn(menu);
        assertEquals(menuService.create(menuDTO),menu);
    }

    @Test
    void testCreateMenuException(){
        MenuDTO menuDTO = new MenuDTO();
        menuDTO.setId(1L);
        menuDTO.setUrl("");
        menuDTO.setName("Root");
        menuDTO.setParentId(null);
        when(menuRepository.save(any())).thenThrow(new RuntimeException());
        assertThrows(RuntimeException.class,()->menuService.create(menuDTO));
    }

}
