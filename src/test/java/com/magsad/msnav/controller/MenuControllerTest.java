package com.magsad.msnav.controller;

import com.magsad.msnav.entity.Menu;
import com.magsad.msnav.exception.MenuNotFoundException;
import com.magsad.msnav.payload.dto.MenuDTO;
import com.magsad.msnav.service.MenuService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MenuControllerTest {

    @Mock
    private MenuService menuService;

    @InjectMocks
    private MenuController menuController;

    @Test
    void testObjectNotNull(){
        assertNotNull(menuController);
    }

    @Test
    void testGetAllMenusSuccess(){
        when(menuService.getAll()).thenReturn(getMenuList());
        List<Menu> menus = menuController.getAllMenus();
        assertEquals(menus.size(),3);
    }

    List<Menu> getMenuList(){
        List<Menu> menuList = new ArrayList<>();
        Menu menu1 = new Menu();
        Menu menu2 = new Menu();
        Menu menu3 = new Menu();

        menuList.add(menu1);
        menuList.add(menu2);
        menuList.add(menu3);
        return menuList;
    }

    @Test
    void testGetMenuByIdSuccess(){
        when(menuService.getById(1L)).thenReturn(getMenu());

        Menu menu = menuController.getMenuById(1L);
        assertEquals(menu.getUrl(),"/");
        assertEquals(menu.getName(),"Root");
    }

    Menu getMenu(){
        Menu menu = new Menu();
        menu.setId(1L);
        menu.setUrl("/");
        menu.setName("Root");
        menu.getChildren().add(menu);
        return menu;
    }

    @Test
    void testGetMenuByIdNotFoundException(){
        when(menuService.getById(1L)).thenThrow(new MenuNotFoundException(1L));
        assertThrows(MenuNotFoundException.class,()->menuService.getById(1L));
    }


}
