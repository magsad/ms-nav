package com.magsad.msnav;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsNavApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsNavApplication.class, args);
    }

}
