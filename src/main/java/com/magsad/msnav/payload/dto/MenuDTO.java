package com.magsad.msnav.payload.dto;

import lombok.*;

@Data
public class MenuDTO {
    private Long id;
    private String url;
    private String name;
    private Long parentId;
}
