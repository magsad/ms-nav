package com.magsad.msnav.controller;

import com.magsad.msnav.exception.MenuNotFoundException;
import com.magsad.msnav.payload.response.MessageResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLException;

@RestControllerAdvice
@Slf4j
public class MainRestControllerAdvice {
    @ExceptionHandler(MenuNotFoundException.class)
    public ResponseEntity<MessageResponse> applicationException(MenuNotFoundException e) {
        log.error(MenuNotFoundException.class.toString());
        log.error(e.getMessage());
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setMessage(e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(messageResponse);
    }

    @ExceptionHandler(SQLException.class)
    public ResponseEntity<MessageResponse> applicationException(SQLException e) {
        log.error(SQLException.class.toString());
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setMessage(e.getMessage());
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(messageResponse);
    }

}
