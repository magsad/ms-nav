package com.magsad.msnav.controller;

import com.magsad.msnav.entity.Menu;
import com.magsad.msnav.payload.dto.MenuDTO;
import com.magsad.msnav.service.MenuService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("menu")
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true,level = AccessLevel.PRIVATE)
public class MenuController {
    MenuService menuService;

    @GetMapping
    public List<Menu> getAllMenus(){
        return menuService.getAll();
    }

    @GetMapping("{id}")
    public Menu getMenuById(@PathVariable Long id){
        return menuService.getById(id);
    }

    @GetMapping("query")
    public Menu getMenuByIdQuery(@RequestParam Long id){
        return menuService.getById(id);
    }

    @PostMapping
    public Menu createMenu(@RequestBody MenuDTO menuDTO){
        return menuService.create(menuDTO);
    }

    @PutMapping
    public Menu updateMenu(@RequestBody MenuDTO menuDTO){
        return menuService.update(menuDTO);
    }

    @DeleteMapping("{id}")
    public void deleteMenu(@PathVariable Long id){
        menuService.delete(id);
    }
}
