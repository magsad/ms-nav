package com.magsad.msnav.service;

import com.magsad.msnav.entity.Menu;
import com.magsad.msnav.payload.dto.MenuDTO;
import com.magsad.msnav.exception.MenuNotFoundException;
import com.magsad.msnav.repository.MenuRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true,level = AccessLevel.PRIVATE)
@Transactional
public class MenuService {
    MenuRepository menuRepository;

    public List<Menu> getAll() {
        return menuRepository.findAll()
                .stream()
                .filter(menu-> Objects.isNull(menu.getParent()))
                .collect(Collectors.toList());
    }


    public Menu getById(Long id){
        return menuRepository.findById(id).orElseThrow(() -> new MenuNotFoundException(id));
    }

    public Menu create(MenuDTO menuDTO){
        Menu menu = new Menu();
        menu.setUrl("/"+ menuDTO.getUrl());
        menu.setName(menuDTO.getName());
        if (menuDTO.getUrl().equals("")){
            menu.setParent(null);
        }else {
            menu.setParent(menuRepository.findById(menuDTO.getParentId()).orElseThrow(() -> new MenuNotFoundException(menuDTO.getId())));
        }
        return menuRepository.save(menu);
    }

    public Menu update(MenuDTO menuDTO){
        Menu menuForUpdate = menuRepository.findById(menuDTO.getId()).orElseThrow(() -> new MenuNotFoundException(menuDTO.getId()));
        menuForUpdate.setUrl("/"+ menuDTO.getUrl());
        menuForUpdate.setName(menuDTO.getName());
        menuForUpdate.setParent(menuRepository.findById(menuDTO.getParentId()).get());
        return menuRepository.save(menuForUpdate);
    }

    public void delete(Long id) {
        List<Menu> children = menuRepository.findByParent(menuRepository.findById(id).get());
        menuRepository.deleteAll(children);
        menuRepository.deleteById(id);
    }
}
