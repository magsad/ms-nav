package com.magsad.msnav.exception;

public class MenuNotFoundException extends RuntimeException{
    public MenuNotFoundException(Long id) {
        super(String.format("Menu not found with id = %d",id));
    }
}
